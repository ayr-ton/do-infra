provider "digitalocean" {
  token   = var.token
}

resource "digitalocean_kubernetes_cluster" "kubernetes_cluster" {
  name    = "k8s-csd"
  region  = "ams3"
  version = "1.19.3-do.3"

  node_pool {
    name       = "pool-csd"
    size       = "s-1vcpu-2gb"
    auto_scale = false
    node_count = 2
  }
}