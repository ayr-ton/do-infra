terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "Knowledge21"

    workspaces {
      name = "do-infra"
    }
  }
}

module "k8s-csd" {
  source = "./k8s-csd"
  token  = var.digitalocean_token
}

